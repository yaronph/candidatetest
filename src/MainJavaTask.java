
public class MainJavaTask {
	
	//private static int i = 0;
	
	public static void main(String[] args) {
		/*
		 * TODO: Write a recursive function to print reverse of a given string.
		 */
		
		String str = "hello";
		reverseRecursive(str);
		//test kd 23.03 11:30
	}

	public static String reverseRecursive(String originalStr) {
		
		//return reverseRecursive(originalStr.substring(1)) + originalStr.charAt(0);
		
		
		//int localI = i++;
        if ((null == originalStr) || (originalStr.length()  <= 1)) {
            return originalStr;
        }
        System.out.println("Step " /*+ localI */+ ": " + originalStr.substring(1) + " / " + originalStr.charAt(0));
        String reversed = reverseRecursive(originalStr.substring(1)) + originalStr.charAt(0);

        System.out.println("Step " /*+ localI*/ + " returns: " + reversed);
        return reversed;
		
	}

}
