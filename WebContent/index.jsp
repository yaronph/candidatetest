<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.12.4.js"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="displayAlertDialog.js"></script>  
	<!--  
	TODO:
    The purpose is to use JQuery alert instead of JavaScript alert([message]).
    
    Please implement displayAlertDialog([alert message], [alert title], [focus id (optional)]) by Writing in 'displayAlertDialog.js' your extension code to JQuery ui dialog (http://api.jqueryui.com/dialog/).
    
    Rules:
    * When the dialog is open the user cannot interact with the screen "behind" it
    * If [focus id] is passed (as in the case below), the empty text box should get the focus when the alert is closed.
    * No extra code or html should be added to this page (all of the code should be written in the 'displayAlertDialog.js' file, where you might need to generate html code using javascript)
       
    Good Luck!
	-->
	
	<title>
		Test
	</title>
	<style type="text/css">
    </style>
	<script type="text/javascript">  
		function check() {
			/* if($('#txt1').val() == '') { 
				displayAlertDialog("Please enter value", "Alert!", "txt1"); 
				return; 
			}  */
		} 
	</script>
</head>
<body>
	<input type="text"  id="txt1" size="15" > 
	<button type="button" id="b1" onclick="check()">Check not empty!!!</button>
</body>
</html>